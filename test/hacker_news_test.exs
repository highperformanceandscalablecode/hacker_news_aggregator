defmodule HackerNewsAggregator.HackerNewsTest do
  use ExUnit.Case

  alias HackerNewsAggregator.HackerNews

  test "get a story test" do
    HackerNews.update_cache()

    story_id = List.first(Cachex.get!(:cache, "ids"))
    story = Cachex.get!(:cache, story_id)

    assert story["type"] == "story"
  end
end
