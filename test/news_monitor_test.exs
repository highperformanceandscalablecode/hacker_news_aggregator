defmodule HackerNewsAggregator.NewsMonitorTest do
  use ExUnit.Case

  alias HackerNewsAggregator.NewsMonitor

  test "accepts a time in seconds on start" do
    time_in_seconds = 10
    assert {:ok, _pid} = NewsMonitor.start_link(time_in_seconds)
  end
end
