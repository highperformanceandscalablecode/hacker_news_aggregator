defmodule HackerNewsAggregatorWeb.StoryControllerTest do
  use HackerNewsAggregatorWeb.ConnCase

  test "GET /api/stories", %{conn: conn} do
    conn = get(conn, "/api/stories?page=1")
    %{"items" => items} = json_response(conn, 200)

    assert %{
             "by" => _,
             "descendants" => _,
             "id" => _,
             "kids" => _,
             "score" => _,
             "time" => _,
             "title" => _,
             "type" => "story"
           } = List.first(items)
  end

  test "GET /api/stories/:id", %{conn: conn} do
    conn = get(conn, "/api/stories?page=1")
    %{"items" => items} = json_response(conn, 200)

    id = items |> List.first() |> Map.get("id")

    conn = get(conn, "/api/stories/#{id}")
    assert %{} = json_response(conn, 200)
  end
end
