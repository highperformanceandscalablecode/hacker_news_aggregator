defmodule HackerNewsAggregatorWeb.NewsChannelTest do
  use HackerNewsAggregatorWeb.ChannelCase

  setup do
    {:ok, _, socket} =
      HackerNewsAggregatorWeb.NewsSocket
      |> socket("user_id", %{some: :assign})
      |> subscribe_and_join(HackerNewsAggregatorWeb.NewsChannel, "news:stories")

    %{socket: socket}
  end

  test "notify new stories", %{socket: socket} do
    push(socket, "new_stories", %{
      "stories" => [
        %{
          "by" => "graderjs",
          "descendants" => 84,
          "id" => 30_206_278,
          "kids" => [30_206_874],
          "score" => 119,
          "time" => 1_643_979_841,
          "title" => "Don't allow animated favicons (2001)",
          "type" => "story",
          "url" => "https://bugzilla.mozilla.org/show_bug.cgi?id=111373"
        }
      ]
    })

    assert_broadcast "stories", %{
      "stories" => [
        %{
          "by" => "graderjs",
          "descendants" => 84,
          "id" => 30_206_278,
          "kids" => [30_206_874],
          "score" => 119,
          "time" => 1_643_979_841,
          "title" => "Don't allow animated favicons (2001)",
          "type" => "story",
          "url" => "https://bugzilla.mozilla.org/show_bug.cgi?id=111373"
        }
      ]
    }
  end
end
