# HackerNewsAggregator

## Overview


Hacker News Aggregator is a cache mecanism that consumes data from Hacker News API(https://github.com/HackerNews/API), focusing on stories.


## Features


- Monitor Hacker News top stories
- Exposes stories via two APIs: JSON over http and JSON over WebSockets


## Development


### Get dependencies
````
$ mix deps.get
````

### Compile dependencies
````
$ mix deps.compile
````

### Start Application

````
$ mix phx.server
```

### Example requests with CURL

**Get stories**
```bash
curl http://localhost:4000/api/stories?page=1
```

**Get story**
```bash
curl http://localhost:4000/api/stories/30215210
```
