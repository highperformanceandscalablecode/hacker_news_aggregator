defmodule HackerNewsAggregator.News do
  @moduledoc """
  The News context.
  """

  @doc """
  Returns the list of ids of the stories.
  """
  @spec list_stories_ids() :: [integer]
  def list_stories_ids do
    Cachex.get!(:cache, "ids")
  end

  @doc """
  Returns a story or it returns null if `id` is not present.
  """
  @spec get_story!(integer) :: map | nil
  def get_story!(id) do
    Cachex.get!(:cache, id)
  end

  @doc """
  Returns a story.
  """
  @spec get_story(integer) :: {:ok, map} | {:error, any}
  def get_story(id) do
    case Cachex.get(:cache, id) do
      {:ok, nil} -> {:error, :not_found}
      {:ok, item} -> {:ok, item}
    end
  end

  @doc """
  Returns the list of stories.
  """
  @spec list_stories() :: [map]
  def list_stories do
    Enum.map(list_stories_ids(), fn id -> get_story!(id) end)
  end
end
