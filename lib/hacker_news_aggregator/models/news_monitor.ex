defmodule HackerNewsAggregator.NewsMonitor do
  @moduledoc """
  Monitor for Hacker News
  """
  use GenServer

  alias HackerNewsAggregator.HackerNews
  alias HackerNewsAggregatorWeb.Endpoint, as: NewsSocket

  def start_link(time_in_seconds) do
    GenServer.start_link(__MODULE__, time_in_seconds)
  end

  @impl true
  def init(time_in_seconds) do
    HackerNews.update_cache()
    refresh(time_in_seconds)
    {:ok, %{time: time_in_seconds}}
  end

  @impl true
  def handle_info(:refresh, state) do
    %{time: time_in_seconds} = state
    HackerNews.update_cache()
    notify()
    refresh(time_in_seconds)
    {:noreply, %{time: time_in_seconds}}
  end

  defp refresh(time_in_seconds) do
    Process.send_after(self(), :refresh, time_in_seconds * 1000)
  end

  defp notify do
    stories = HackerNewsAggregator.News.list_stories()
    NewsSocket.broadcast("news:stories", "new_stories", %{"stories" => stories})
  end
end
