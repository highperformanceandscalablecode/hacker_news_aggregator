defmodule HackerNewsAggregator.HackerNews do
  @moduledoc """
  Hacker News Reader
  """

  require Logger

  @doc """
    Update cache with the latest stories
  """
  @spec update_cache() :: {:ok, true} | {:error, atom}
  def update_cache do
    # get ids and insert item in cache
    ids =
      Enum.map(get_items_by_type("story", 50), fn item ->
        Cachex.put!(:cache, item["id"], item)
        item["id"]
      end)

    Cachex.put!(:cache, "ids", ids)
  end

  defp get_items_by_type(type, limit) do
    Enum.reduce_while(get_item_ids(), [], fn id, acc ->
      item = get_item_by_id(id)

      acc =
        if item["type"] == type do
          [item | acc]
        else
          acc
        end

      if length(acc) < limit do
        {:cont, acc}
      else
        {:halt, acc}
      end
    end)
  end

  defp get_item_ids do
    case HTTPoison.get("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body
        |> Jason.decode!()
        |> Enum.sort()

      {:ok, %HTTPoison.Response{status_code: 404}} ->
        Logger.info("Not found :(")
        []

      {:error, %HTTPoison.Error{reason: reason}} ->
        Logger.error(reason)
        []
    end
  end

  defp get_item_by_id(id) do
    case HTTPoison.get("https://hacker-news.firebaseio.com/v0/item/#{id}.json?print=pretty") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        Jason.decode!(body)

      {:ok, %HTTPoison.Response{status_code: 404}} ->
        Logger.info("Not found :(")
        Map.new()

      {:error, %HTTPoison.Error{reason: reason}} ->
        Logger.error(reason)
        Map.new()
    end
  end
end
