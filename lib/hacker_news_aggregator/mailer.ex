defmodule HackerNewsAggregator.Mailer do
  @moduledoc false
  use Swoosh.Mailer, otp_app: :hacker_news_aggregator
end
