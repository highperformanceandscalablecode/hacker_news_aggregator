defmodule HackerNewsAggregatorWeb.StoryController do
  use HackerNewsAggregatorWeb, :controller

  alias HackerNewsAggregator.News

  action_fallback HackerNewsAggregatorWeb.FallbackController

  @spec index(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def index(conn, params) do
    config = maybe_put_default_config(params)

    stories =
      News.list_stories_ids()
      |> Scrivener.paginate(config)
      |> get_stories()

    render(conn, "index.json", stories: stories)
  end

  @spec show(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def show(conn, %{"id" => id}) do
    with {:ok, id} <- parse_id(id),
         {:ok, story} <- News.get_story(id) do
      render(conn, "show.json", story: story)
    end
  end

  defp maybe_put_default_config(%{"page" => _page_number} = params),
    do: Map.put(params, "page_size", 10)

  defp maybe_put_default_config(_params), do: %Scrivener.Config{page_number: 1, page_size: 10}

  defp get_stories(%{entries: ids}) do
    Enum.map(ids, fn id -> News.get_story!(id) end)
  end

  defp parse_id(id) do
    case Integer.parse(id) do
      {value, ""} -> {:ok, value}
      _ -> {:error, "invalid id"}
    end
  end
end
