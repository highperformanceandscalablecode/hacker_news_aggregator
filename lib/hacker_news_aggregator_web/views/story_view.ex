defmodule HackerNewsAggregatorWeb.StoryView do
  use HackerNewsAggregatorWeb, :view

  alias HackerNewsAggregatorWeb.StoryView

  @spec render(String.t(), map) :: map
  def render("index.json", %{stories: stories}) do
    %{items: render_many(stories, StoryView, "story.json")}
  end

  @spec render(String.t(), map) :: map
  def render("show.json", %{story: story}) do
    render_one(story, StoryView, "story.json")
  end

  @spec render(String.t(), map) :: map
  def render("story.json", %{story: story}) do
    story
  end
end
