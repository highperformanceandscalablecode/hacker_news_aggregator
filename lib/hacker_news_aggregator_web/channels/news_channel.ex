defmodule HackerNewsAggregatorWeb.NewsChannel do
  @moduledoc """
  News Channel
  """
  use HackerNewsAggregatorWeb, :channel

  alias HackerNewsAggregator.News

  @impl true
  def join("news:stories", _payload, socket) do
    send(self(), :after_join)
    {:ok, socket}
  end

  @impl true
  def join("news:stories" <> _private_room_id, _payload, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  @impl true
  def handle_info(:after_join, socket) do
    stories = News.list_stories()
    push(socket, "join", %{stories: stories})
    {:noreply, socket}
  end

  @impl true
  def handle_in("new_stories", %{"stories" => stories}, socket) do
    broadcast!(socket, "stories", %{"stories" => stories})
    {:noreply, socket}
  end
end
